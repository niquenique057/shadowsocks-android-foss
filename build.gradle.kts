// Top-level build file where you can add configuration options common to all sub-projects/modules.

plugins {
    id("com.github.ben-manes.versions") version "0.33.0"
}

buildscript {
    apply(from = "repositories.gradle.kts")

    repositories {
        google()
        jcenter()
        maven("https://plugins.gradle.org/m2/")
    }

    dependencies {
        val kotlinVersion = rootProject.extra.get("kotlinVersion").toString()
        classpath(rootProject.extra.get("androidPlugin").toString())
        classpath(kotlin("gradle-plugin", kotlinVersion))
        classpath("com.vanniktech:gradle-maven-publish-plugin:0.13.0")
        classpath("gradle.plugin.org.mozilla.rust-android-gradle:plugin:0.8.3")
        classpath("org.jetbrains.dokka:dokka-gradle-plugin:$kotlinVersion")
    }
}

allprojects {
    apply(from = "${rootProject.projectDir}/repositories.gradle.kts")
}

tasks.register<Delete>("clean") {
    delete(rootProject.buildDir)
}
